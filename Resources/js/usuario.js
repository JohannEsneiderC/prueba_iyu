$(document).ready(function() {
    $('#user_datatable').dataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
function search_usu(search_type){

    var document = $("#txt_usuario").val();

    if(isNaN(document)){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'El campo número de identificación debe ser númerico',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
          return false;
    }else if(document === '' || document === null){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'El campo número de identificación es obligatorio',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
          return false;
    }else{
    
        $('#usuario_tab').html('<tr><td colspan="7" id="carg_tbl_det" style="text-align: center;"><img src="../../Resources/Image/load.gif" width="100" /></td></tr>');
        $.ajax({
            type: 'POST',
            url: 'Ajax/usuario.php?action=get_user',
            dataType: "JSON",
            data: "document="+document,
            success: function(data) {
                
                if (data == '' || data == null) {
                    $('#usuario_tab').html('<tr class="alt" style="background-color: rgb(243, 155, 136);"><td colspan="7">No se encontró el número de identificación.</td></tr>');
                } else {
                    if(search_type != 1){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: '¡Usuario encontrado!',
                            showConfirmButton: false,
                            timer: 1800
                          });
                    }
                    $.each(data, function(i, valor) {
                        var col = "";
                        if (i % 2 == 0) {
                            col = "class='alt'";
                        } else {
                            col = "";
                        }
                        $('#usuario_tab tr:last').after('<tr ' + col + ' ><td>' + valor.usua_nomb + '</td><td>' + valor.usua_login + '</td><td>' + valor.nombre_dependencia + '</td><td>' + valor.perfil + '</td><td>' + valor.user_state + '</td><td>' + valor.usua_email + '</td><td><button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#usuario_modal" onClick="update_user('+valor.id+');">Editar</button></td></tr>');
                    });
                }
                $('#carg_tbl_det').css('display', 'none');
            }

        }); 
    }
}
function update_user(user_id){
    
    $("#id_usu").val(user_id);

        $.ajax({
            type: "POST",
            url: 'Ajax/usuario.php?action=edit',
            dataType: "JSON",
            data: "Id=" + user_id,
            success: function(data) {

                $.each(data, function(i, valor) {

                    $("#name").val(valor.usua_nomb);
                    $("#username").val(valor.usua_login);
                    $("#cod_dep").val(valor.nombre_dependencia);
                    $("#cod_usu").val(valor.perfil);
                    $("#estado_usu").val(valor.user_state);
                    $("#email_usu").val(valor.usua_email);
                });
            }
        });
}
function save_data(){
    var email = $("#email_usu").val();

    var correct_email = email_validate(email);

    //Si el email es correcto(true)
    if(correct_email === true){
        
        if(email === '' || email === null){
            Swal.fire({
                icon: 'error',
                title: 'Ocurrió un error en el servidor',
                text: 'El campo correo electrónico es obligatorio',
                footer: '<h3>Johann Developer &copy;</h3>'
              })
              return false;
        }else{
            $.ajax({
                type: "POST",
                url: 'Ajax/usuario.php?action=update',
                data: $("#user_form").serialize(), // Adjuntar los campos del formulario enviado.
                success: function(data) {
                    if (data == 2) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: '¡Usuario modificado exitosamente!',
                            showConfirmButton: false,
                            timer: 1800
                          });
                          search_usu(1);
                    }else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Ocurrió un error en el servidor',
                            text: 'No se guardaron los datos',
                            footer: '<h3>Johann Developer &copy;</h3>'
                          })
                    }
                }
            });
        }
    }else{ //Si el email no es correcto(false)
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'La dirección de correo electrónico no es válida',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
    }
}
//Se valida con una expresión regular que el formato del correo electrónico sea correcto.
function email_validate(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
  }