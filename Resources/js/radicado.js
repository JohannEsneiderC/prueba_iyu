$(document).ready(function() {
    $('#radicado_datatable').dataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
function search_rad(search_type){
    var txt_rad = $("#txt_radicado").val();

    if(txt_rad.length >= 15){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'Este campo solo permite buscar 14 digitos',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
          return false;

    }else if(isNaN(txt_rad)){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'El campo número de radicado debe ser númerico',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
          return false;
    }else if(txt_rad === '' || txt_rad === null){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'El campo número de radicado es obligatorio',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
          return false;
    }else{
        $('#radicado_tab').html('<tr><td colspan="8" id="carg_tbl_det" style="text-align: center;"><img src="../../Resources/Image/load.gif" width="100" /></td></tr>');
        $.ajax({
            type: 'POST',
            url: 'Ajax/radicado.php?action=get_radicado',
            dataType: "JSON",
            data: "radicado="+txt_rad,
            success: function(data) {
            
                if (data == '' || data == null) {
                    $('#radicado_tab').html('<tr class="alt"><td style="background-color: rgb(243, 155, 136);" colspan="8">No se encontró el número de radicado</td></tr>');

                } else {
                    if(search_type != 1){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: '¡Radicado encontrado!',
                            showConfirmButton: false,
                            timer: 1800
                          });
                    }

                    $.each(data, function(i, valor) {
                        var col = "";
                        if (i % 2 == 0) {
                            col = "class='alt'";
                        } else {
                            col = "";
                        }
                        $('#radicado_tab tr:last').after('<tr ' + col + ' ><td>' + valor.radi_fech_radi + '</td><td>' + valor.ra_asun + '</td><td>' + valor.document_type + '</td><td>' + valor.radi_nume_radi + '</td><td>' + valor.r_nombre + '</td><td>' + valor.r_direccion + '</td><td>' + valor.r_telefono + '</td><td><button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#radicado_modal" onClick="update_rad('+valor.id+');">Editar</button></td></tr>');
                    });
                }
                $('#carg_tbl_det').css('display', 'none');
            }
        });
    }
}

function update_rad(id_radicado){

        $("#id_rad").val(id_radicado);

        $.ajax({
            type: "POST",
            url: 'Ajax/radicado.php?action=edit',
            dataType: "JSON",
            data: "Id=" + id_radicado,
            success: function(data) {

                $.each(data, function(i, valor) {

                    $("#f_radicado").val(valor.radi_fech_radi);
                    $("#asunto").val(valor.ra_asun);
                    $("#t_document").val(valor.document_type);
                    $("#n_radicado").val(valor.radi_nume_radi);
                    $("#n_remitente").val(valor.r_nombre);
                    $("#d_remitente").val(valor.r_direccion);
                    $("#t_remitente").val(valor.r_telefono);
                });
            }
        }); // fin ajax
}

function save_data(){
    var subject = $("#asunto").val();
    if(subject === '' || subject === null){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'El campo asunto de radicado es obligatorio',
            footer: '<h3>Johann Developer &copy;</h3>'
          })
          return false;
    }else if(subject.length >= 13){
            Swal.fire({
                icon: 'error',
                title: 'Ocurrió un error en el servidor',
                text: 'Este campo solo permite almacenar 12 digitos alfanuméricos',
                footer: '<h3>Johann Developer &copy;</h3>'
            })
        return false;  
    }else if(subject === 'Sin información' || subject === 'Sin informacion'){
        Swal.fire({
            icon: 'error',
            title: 'Ocurrió un error en el servidor',
            text: 'El dato <Sin información> no está permitido para el campo asunto',
            footer: '<h3>Johann Developer &copy;</h3>'
        })
    return false; 
    }else{
        $.ajax({
            type: "POST",
            url: 'Ajax/radicado.php?action=update',
            data: $("#radicado_form").serialize(), // Adjuntar los campos del formulario enviado.
            success: function(data) {
                if (data == 2) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡Radicado modificado exitosamente!',
                        showConfirmButton: false,
                        timer: 1800
                      });
                      search_rad(1);
                    }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Ocurrió un error en el servidor',
                        text: 'No se guardaron los datos',
                        footer: '<h3>Johann Developer &copy;</h3>'
                      });
                      search_rad();
                }
            }
        });
    }
}