$(document).ready(function() {
    $('#dependencies_datatable').dataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

    $.post('Ajax/dependencia.php',{action:'get_dependencies'},function(data){
        $("#dep_code").html(data);
    });
});

function search_dep(){
     var dependencie = $("#dep_code").val();
     
     $('#dep_tab').html('<tr><td colspan="7" id="carg_tbl_det" style="text-align: center;"><img src="../../Resources/Image/load.gif" width="100" /></td></tr>');
        $.ajax({
            type: 'POST',
            url: 'Ajax/dependencia.php?action=get_users',
            dataType: "JSON",
            data: "dependencia="+dependencie,
            success: function(data) {
                
                if (data == '' || data == null) {
                    $('#dep_tab').html('<tr class="alt"><td colspan="7" style="background-color: rgb(243, 155, 136);">No se encontraron datos para esta dependencia.</td></tr>');

                } else {

                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: '¡Usuarios encontrados!',
                        showConfirmButton: false,
                        timer: 1000
                      })

                    $.each(data, function(i, valor) {
                        var col = "";
                        if (i % 2 == 0) {
                            col = "class='alt'";
                        } else {
                            col = "";
                        }
                        $('#dep_tab tr:last').after('<tr ' + col + ' ><td>' + valor.usua_nomb + '</td><td>' + valor.usua_login + '</td><td>' + valor.nombre_dependencia + '</td><td>' + valor.perfil + '</td><td>' + valor.user_state + '</td></tr>');
                    });
                }
                $('#carg_tbl_det').css('display', 'none');
            }

        }); 
}

function active_btn(){
    document.getElementById('search_btn').disabled = false;
}